package  bomberman.entities.tile.item;
/**
 * Class Item
 * @author Duog Hai Minh, Le Khanh Toan
 * @version 1.0
 * @since 2018-11-10
 */


import  bomberman.entities.tile.Tile;
import  bomberman.graphics.Sprite;

public abstract class Item extends Tile {
	
	public Item(int x, int y, Sprite sprite) {
		super(x, y, sprite);
	}
}
