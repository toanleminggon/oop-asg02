package  bomberman.entities.tile.item;
/**
 * Class FlameItem
 * @author Duog Hai Minh, Le Khanh Toan
 * @version 1.0
 * @since 2018-11-10
 */
import bomberman.Game;
import  bomberman.entities.Entity;
import bomberman.entities.character.Bomber;
import  bomberman.graphics.Sprite;


public class FlameItem extends Item {

	public FlameItem(int x, int y, Sprite sprite) {
		super(x, y, sprite);
	}

	@Override
	public boolean collide(Entity e) {
		if(isRemoved()) return false;
		
		if(e instanceof Bomber) {
			Game.addBombRadius(1);
			remove();
			return true;
		}
		return false;
	}

}
